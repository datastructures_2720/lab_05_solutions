import java.util.List;

import objects.DLine;
import sortinterface.ISortMethods;

/**
 * This class should implement the interface 'ISortMethods<DLine>'
 * 
 * @author Azim Ahmadzadeh [https://grid.cs.gsu.edu/~aahmadzadeh1/]
 *
 */
public class SortMethods implements ISortMethods<DLine> {

	@Override
	public void sort_1(List<DLine> ls) {
		/* - - - - - - - - - - - - *
		 * - - INSERTION SORT- - - *
		 * - - - - - - - - - - - - */
//		this.displayList(ls);
//		System.out.println();
		
		int i = 0, j = 0;
		DLine tmp = new DLine();
		
		while(i < ls.size()) {
			tmp = new DLine(ls.get(i));
			j = i - 1;
			
			while(j >= 0 && (ls.get(j).compareTo(tmp) > 0)) {
				ls.set(j+1, new DLine(ls.get(j)));
				j = j - 1;
//				this.displayList(ls);
			}
			ls.set(j+1, tmp);
			i = i + 1;
//			this.displayList(ls);
		}
	}

	@Override
	public void sort_2(List<DLine> ls) {
		/* - - - - - - - - - - - - *
		 * - - - BUBBLE SORT - - - *
		 * - - - - - - - - - - - - */
//		this.displayList(ls);
//		System.out.println();
		
		int n = ls.size();
		int nn = 0;
		DLine tmp = new DLine();
		while(n > 0) {
			nn = 0;
			for(int i = 1; i <= n - 1; i++) {
				if(ls.get(i-1).compareTo(ls.get(i)) > 0) {
					tmp = new DLine(ls.get(i-1));
					ls.set(i-1, new DLine(ls.get(i)));
					ls.set(i, tmp);
					nn = i;
				}
//				this.displayList(ls);
			}
			n = nn;
//			this.displayList(ls);
		}
		
	}

	@Override
	public void sort_3(List<DLine> ls) {
		/* - - - - - - - - - - - - - *
		 * - - - SELECTION SORT- - - *
		 * - - - - - - - - - - - - - */
//		this.displayList(ls);
//		System.out.println();
		
		int n = ls.size();
		int min = 0;
		DLine tmp = new DLine();
		
		for(int i = 0; i < n-1; i++) {
			min = i;
			for(int j = i+1; j < n; j++) {
				if(ls.get(j).compareTo(ls.get(min)) < 0 )
					min = j;
//				this.displayList(ls);
			}
			
			if(min != i) {
				tmp = new DLine(ls.get(i));
				ls.set(i, new DLine(ls.get(min)));
				ls.set(min, tmp);
			}
//			this.displayList(ls);
		}
	}
}
