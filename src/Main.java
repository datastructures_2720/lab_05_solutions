import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import objects.DLine;
import sortinterface.ISortMethods;

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * -> Class: Data Structures - 2720 - - - - - - - - - - - - - - - - - - - - - -
 * -> LAB: 05 [Solutions] - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Date: Friday 21 Sep, 2018 - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Subject: Sorting - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Lab Web-page: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * [https://sites.google.com/view/azimahmadzadeh/teaching/data-structures-2720]
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * 
 * @author Azim Ahmadzadeh [https://grid.cs.gsu.edu/~aahmadzadeh1/] - - - - -
 */
public class Main {

	public static void main(String[] args) {

		/* ------------ Preparation ------------ */
		// -Create an object of type SortMethods with a reference to ISortMethods,
		// -Create an empty list called 'myList' (sorting should NOT be done on this),
		// -Populate 'myList' with DLines of the following lengths:
		// {15,12,9,6,13,10,7,1},
		// -Create another empty list called 'sortedList' (We do our sorting on this),
		// -Populate 'sortedList' with the same elements, (Hint: Use the method:
		// 'allAll()')
		// -Display both of your lists using 'displayList' and make sure they are
		// correctly populated.
		ISortMethods<DLine> sm = new SortMethods();
		List<DLine> myList = new LinkedList<>();
		myList.addAll(
				Arrays.asList(new DLine(20), new DLine(10), new DLine(5), new DLine(15), new DLine(3), new DLine(1)));
		List<DLine> sortedList = new LinkedList<>();
		sortedList.addAll(myList);
		sm.displayList(myList);
		/* ------------ End Prep ------------ */
		//
		//
		//
		//
		//
		/* ------------ Sort I ------------ */startSection(1);
		// TASK:
		// -Implement the method 'sort_1' in the class 'SortMethods' by following its
		// javadoc,
		// -Use 'sort_1' to sort 'sortedList' in place,
		// -Display your sorting steps by calling 'displayList' inside your sort method,
		// -Verify the correctness of your algorithm by visually checking the steps and
		// the final list.
		// TODO:
		sm.sort_1(sortedList);
		sm.displayList(sortedList);
		// -Q: Which sort algorithm do you think this is? [Insertion | Selection |
		// Bubble]
		// YOUR ANSWER:_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
		// _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
		/* ------------ End I ------------ */endSection(1);
		//
		//
		//
		//
		//
		/* ------------ Sort II ------------ */startSection(2);
		// TASK:
		// -Implement the method 'sort_2' in the class 'SortMethods' by followings its
		// javadoc,
		// -Clear 'sortedList' and again add all elements to it.
		// -Use 'sort_2' to sort 'sortedList' in place,
		// -Display your sorting steps by calling 'displayList' inside your sort method,
		// -Verify your algorithm by visually checking the steps and the final list.
		// TODO:
		sortedList.clear();
		sortedList.addAll(myList);
		sm.sort_2(sortedList);
		sm.displayList(sortedList);
		// -Q: Which sort algorithm do you think this is? [Insertion | Selection |
		// Bubble]
		// YOUR ANSWER:_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
		// _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
		/* ------------ End II ------------ */endSection(2);
		//
		//
		//
		//
		//
		/* ------------ Sort III ------------ */startSection(3);
		// TASK:
		// -Implement the method 'sort_2' in the class 'SortMethods' by followings its
		// javadoc,
		// -Clear 'sortedList' and again add all elements to it.
		// -Use 'sort_3' to sort 'sortedList' in place,
		// -Display your sorting steps by calling 'displayList' inside your sort method,
		// -Verify your algorithm by visually checking the steps and the final list.
		// TODO:
		sortedList.clear();
		sortedList.addAll(myList);
		sm.sort_3(sortedList);
		sm.displayList(sortedList);
		// -Q: Which sort algorithm do you think this is? [Insertion | Selection |
		// Bubble]
		// YOUR ANSWER:_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
		// _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
		/* ------------ End III ------------ */endSection(3);
		//
		//
		//
		//
		//
		/* ------------ Experiment IV ------------ */startSection(4);
		// TASK:
		// - [Comment out all calls of 'displayList' in your sort methods!]
		// - Create a new list called 'largeList'
		// - Populate it will 1000 DLines, with the following lengths: {1000, 999, ...,
		// 1}
		// - Run the three sorting methods on 'largeList', and record their execution
		// times, separately.
		// - [Make sure you give each of the methods the unsorted list]
		// TODO
		long startTime, endTime;
		List<DLine> largeList = new LinkedList<>();
		for (int i = 1000; i > 0; i--) {
			largeList.add(new DLine(i));
		}
		sortedList.clear();
		sortedList.addAll(largeList);
		startTime = System.nanoTime();
		sm.sort_1(sortedList);
		endTime = System.nanoTime();
		System.out.println("[sort_1][Worst case] is done in [" + (endTime - startTime) + "] nanoseconds.");

		sortedList.clear();
		sortedList.addAll(largeList);
		startTime = System.nanoTime();
		sm.sort_2(sortedList);
		endTime = System.nanoTime();
		System.out.println("[sort_2][Worst case] is done in [" + (endTime - startTime) + "] nanoseconds.");

		sortedList.clear();
		sortedList.addAll(largeList);
		startTime = System.nanoTime();
		sm.sort_3(sortedList);
		endTime = System.nanoTime();
		System.out.println("[sort_3][Worst case] is done in [" + (endTime - startTime) + "] nanoseconds.");
		// Q: Which ons is faster? Write down the exact execution times in miliseconds.
		// YOUR ANSWER:_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
		// _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
		/* ------------ End IV ------------ */endSection(4);

		/* ------------ Experiment V ------------ */startSection(5);
		// TASK:
		// - [Comment out all calls of 'displayList' in your sort methods!]
		// - Create a new list called 'largeList'
		// - Populate it will 1000 DLines, with the following lengths: {1, 2, ..., 1000}
		// - Run the three sorting methods on 'largeList', and record their execution
		// times, separately.
		// - [Make sure you give each of the methods the unsorted list]
		largeList = new LinkedList<>();
		for (int i = 1; i < 1000; i++) {
			largeList.add(new DLine(i));
		}
		sortedList.clear();
		sortedList.addAll(largeList);
		startTime = System.nanoTime();
		sm.sort_1(sortedList);
		endTime = System.nanoTime();
		System.out.println("[sort_1][Already sorted] is done in [" + (endTime - startTime) + "] nanoseconds.");

		sortedList.clear();
		sortedList.addAll(largeList);
		startTime = System.nanoTime();
		sm.sort_2(sortedList);
		endTime = System.nanoTime();
		System.out.println("[sort_2][Already sorted] is done in [" + (endTime - startTime) + "] nanoseconds.");

		sortedList.clear();
		sortedList.addAll(largeList);
		startTime = System.nanoTime();
		sm.sort_3(sortedList);
		endTime = System.nanoTime();
		System.out.println("[sort_3][Already sorted] is done in [" + (endTime - startTime) + "] nanoseconds.");
		// Q: Which ons is faster? Write down the exact execution times in miliseconds.
		// YOUR ANSWER:_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
		// _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
		/* ------------ End V ------------ */endSection(5);
		//
		//
		//
		//
		//
		//
		/* ------------- CONCLUSION ------------------------------ */
		/* -------------------------------------------------------- */
		/* -- Q: What is your conclusion from the two experiments? */
		/* -- [Write a short paragraph here.] --------------------- */
		/* -------------------------------------------------------- */
		/* -------------------------------------------------------- */
		/* -------------------------------------------------------- */
		/* -------------------------------------------------------- */

	}

	/** IGNORE THIS METHOD **/
	private static void startSection(int i) {
		System.out.print("\n:::::::::::::::::::::::");
		System.out.print(" START [" + i + "] ");
		System.out.print(":::::::::::::::::::::::\n\n");
	}

	/** IGNORE THIS METHOD **/
	private static void endSection(int i) {
		System.out.print("\n________________________");
		System.out.print(" END [" + i + "] ");
		System.out.print("________________________\n\n");
	}

}
